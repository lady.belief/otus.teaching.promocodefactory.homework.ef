using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

public class EfRepository<T>
    : IRepository<T>
    where T: BaseEntity
{
    private readonly PromoContext _promoContext;
    private static bool _isInit = true;

    public EfRepository(PromoContext promoContext)
    {
        _promoContext = promoContext;
        
        if (_isInit) return;
        var data = FakeDataFactory.Data.ContainsKey(typeof(T)) 
            ? FakeDataFactory.Data[typeof(T)] 
            : new List<T>();
        if (data is IEnumerable<T> entities) 
            Init(entities);
        _isInit = true;
    }

    private void Init(IEnumerable<T> data)
    {
        _promoContext.RemoveRange(_promoContext.Set<T>());
        foreach (var entity in data)
        {
            _promoContext.Add<T>(entity);
        }

        _promoContext.SaveChanges();
    }
    
    public Task<IEnumerable<T>> GetAllAsync()
    {
        return Task.FromResult(_promoContext.Set<T>().AsEnumerable());
    }

    public Task<T> GetByIdAsync(Guid id)
    {
        return Task.FromResult(_promoContext.Set<T>().FirstOrDefault(t => t.Id == id));
    }
    
    public Task<Guid> PostAsync(T t)
    {
        _promoContext.Add<T>(t);
        
        _promoContext.SaveChanges();
        
        return Task.FromResult(t.Id);
    }

    public Task<T> PutAsync(T t)
    {
        if(_promoContext.Find<T>(t.Id) != null)
            _promoContext.SaveChanges();
            
        return Task.FromResult(_promoContext.Find<T>(t.Id));
    }

    public Task<Guid> DeleteAsync(Guid id)
    {
        var existed = _promoContext.Find<T>(id);
        if(existed != null)
            _promoContext.Remove<T>(existed);
            
        _promoContext.SaveChanges();

        return Task.FromResult(id);
    }
}