using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess;

public class PromoContext : DbContext
{
    public virtual DbSet<Role> Role { get; set; }
    public virtual DbSet<Employee> Employee { get; set; }
    public virtual DbSet<Customer> Customer { get; set; }
    public virtual DbSet<PromoCode> PromoCode { get; set; }
    public virtual DbSet<Preference> Preference { get; set; }
    public virtual DbSet<CustomerPreference> CustomerPreference { get; set; }

    public PromoContext(DbContextOptions<PromoContext> options) : base(options)
    { }
}