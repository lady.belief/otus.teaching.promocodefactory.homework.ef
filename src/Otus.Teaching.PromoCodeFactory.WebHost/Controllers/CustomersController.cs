﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public CustomersController(IRepository<Customer> customersRepository, 
            IRepository<Preference> preferenceRepository, IRepository<PromoCode> promoCodesRepository,
            IRepository<CustomerPreference> customerPreferenceRepository)
        {
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
            _promoCodesRepository = promoCodesRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
        }
        
        /// <summary>
        /// Получение списка клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            var customersModelList = customers.Select(x => new CustomerShortResponse()
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToList();

            return Ok(customersModelList);
        }
        
        /// <summary>
        /// Получение клиента вместе с выданными ему промомкодами
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);

            if (customer == default)
                return NotFound();

            var customerModel = new CustomerResponse()
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.PromoCodes.Select(x => new PromoCodeShortResponse(x)).ToList(),
                PreferenceNames = _customerPreferenceRepository.GetAllAsync().Result
                    .Where(x => x.CustomerId == customer.Id)
                    .Where(x => _preferenceRepository.GetByIdAsync(x.PreferenceId) != default)
                    .Select(x => _preferenceRepository.GetByIdAsync(x.PreferenceId).Result.Name).ToList()
            };

            return Ok(customerModel);
        }
        
        /// <summary>
        /// Создание нового клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerId = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerId,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreference = CreateCustomerPreferenceList(customerId, request.PreferenceIds)
            };
            var id = await _customersRepository.PostAsync(customer);
            return Ok(id);
        }
        
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == null) return NotFound();

            customer.Email = request.Email;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
                   
            customer.CustomerPreference.Clear();
            customer.CustomerPreference = CreateCustomerPreferenceList(customer.Id, request.PreferenceIds);

            var idCuPrArray = _customerPreferenceRepository.GetAllAsync().Result
                .Where(cp => cp.CustomerId == id)
                .Select(cp => cp.Id).ToArray();
            foreach (var idCuPr in idCuPrArray)
            {
                await _customerPreferenceRepository.DeleteAsync(idCuPr);
            }
            
            await _customersRepository.PutAsync(customer);
            return Ok();

        }
        
        /// <summary>
        /// Удаление клиента вместе с выданными ему промокодами
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer == default) return NotFound();

            var promoCodeIds = _promoCodesRepository.GetAllAsync().Result
                .Where(pr => pr.CustomerId == id)
                .Select(pr => pr.Id).ToArray();
            foreach (var promoCodeId in promoCodeIds)
            {
                await _promoCodesRepository.DeleteAsync(promoCodeId);
            }
            
            await _customersRepository.DeleteAsync(id);

            return Ok();
        }

        private ICollection<CustomerPreference> CreateCustomerPreferenceList(Guid customerId, 
            IEnumerable<Guid> preferenceIds)
        {
            return preferenceIds
                .Select(pr => _preferenceRepository.GetByIdAsync(pr).Result)
                .Where(pr => pr != default)
                .Select(pr => new CustomerPreference()
                {
                    CustomerId = customerId,
                    PreferenceId = pr.Id,
                }).ToList();
        }
    }
}