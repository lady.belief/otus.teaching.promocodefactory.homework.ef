using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers;

/// <summary>
/// Предпочтения
/// </summary>
[ApiController]
[Route("api/v1/[controller]")]
public class PreferenceController : ControllerBase
{
    private readonly IRepository<Preference> _preferenceRepository;
    
    public PreferenceController(IRepository<Preference> preferenceRepository)
    {
        _preferenceRepository = preferenceRepository;
    }

    /// <summary>
    /// Получение списка предпочтений
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<PreferenceResponse>> GetPreferencesAsync()
    {
        return Ok((await _preferenceRepository.GetAllAsync())
            .Select(p => new PreferenceResponse()
            {
                Id = p.Id,
                Name = p.Name
            }).ToArray());
    }
}