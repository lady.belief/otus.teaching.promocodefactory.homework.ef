﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        [Column ("PreferenceName")]
        public string Name { get; set; }
        
        public ICollection<CustomerPreference> CustomerPreference { get; set; }  = new List<CustomerPreference>();
    }
}